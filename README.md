# Entando 4.3.2 Snapshot with entando BPM connector 

## Settings
This docker image exposes port 8080

## Instructions

To start this image you can run:
```bash
docker run --name yourContainerName -p 80:8080 -d 
```


