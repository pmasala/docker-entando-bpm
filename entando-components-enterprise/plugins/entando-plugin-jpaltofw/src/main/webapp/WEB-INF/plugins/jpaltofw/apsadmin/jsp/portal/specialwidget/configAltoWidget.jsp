<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="wp" uri="/aps-core" %>
<%@ taglib prefix="wpsf" uri="/apsadmin-form" %>
<%@ taglib prefix="wpsa" uri="/apsadmin-core" %>

<h1 class="panel panel-default title-page">
    <span class="panel-body display-block">
        <a href="<s:url action="viewTree" namespace="/do/Page" />" title="<s:text name="note.goToSomewhere" />: <s:text name="title.pageManagement" />">
            <s:text name="title.pageManagement" /></a>&#32;/&#32;
        <a href="<s:url action="configure" namespace="/do/Page">
               <s:param name="pageCode"><s:property value="currentPage.code"/></s:param>
           </s:url>" title="<s:text name="note.goToSomewhere" />: <s:text name="title.configPage" />"><s:text name="title.configPage" /></a>&#32;/&#32;
        <s:text name="name.widget" />
    </span>
</h1>

<div id="main" role="main">

    <s:set var="breadcrumbs_pivotPageCode" value="pageCode" />
    <s:include value="/WEB-INF/apsadmin/jsp/portal/include/pageInfo_breadcrumbs.jsp" />

    <s:action namespace="/do/Page" name="printPageDetails" executeResult="true" ignoreContextParams="true"><s:param name="selectedNode" value="pageCode"></s:param></s:action>

    <s:form action="save" namespace="/do/jpaltofw/Page/SpecialWidget/AltoWidget" cssClass="form-horizontal">

        <div class="panel panel-default">
            <div class="panel-heading">
                <s:include value="/WEB-INF/apsadmin/jsp/portal/include/frameInfo.jsp" />
            </div>

            <div class="panel-body">
                <h2 class="h5 margin-small-vertical">
                    <label class="sr-only"><s:text name="name.widget" /></label>
                    <span class="icon fa fa-puzzle-piece" title="<s:text name="name.widget" />"></span>&#32;
                    <s:property value="%{getTitle(widget.type.code, widget.type.titles)}" />
                </h2>
                <p class="sr-only">
                    <wpsf:hidden name="pageCode" />
                    <wpsf:hidden name="frame" />
                    <wpsf:hidden name="widgetTypeCode" value="%{widget.type.code}" />
                    <wpsf:hidden name="validServer" />
                </p>
                <s:if test="hasFieldErrors()">
                    <div class="alert alert-danger alert-dismissable">
                        <button class="close" data-dismiss="alert"><span class="icon fa fa-times"></span></button>
                        <h3 class="h4 margin-none"><s:text name="message.title.FieldErrors" /></h3>
                        <ul>
                            <s:iterator value="fieldErrors">
                                <s:iterator value="value">
                                    <li><s:property escapeHtml="false" /></li>
                                    </s:iterator>
                                </s:iterator>
                        </ul>
                    </div>
                </s:if>

                <fieldset class="margin-large-top">
                    <legend><s:text name="title.serverConfiguration" /></legend>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <label for="baseUrl"><s:text name="label.gokibiBaseUrl" /></label>
                            <s:if test="validServer" >
                                <code><s:property value="%{widget.config.get('baseUrl')}" /></code>
                            </s:if>
                            <s:else>
                                <wpsf:textfield name="baseUrl" id="baseUrl" value="%{widget.config.get('baseUrl')}" cssClass="form-control" />
                            </s:else>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <label for="pid"><s:text name="label.gokibiPid" /></label>
                            <s:if test="validServer" >
                                <code><s:property value="%{widget.config.get('pid')}" /></code>
                            </s:if>
                            <s:else>
                                <wpsf:textfield name="pid" id="pid" value="%{widget.config.get('pid')}" cssClass="form-control" />
                            </s:else>
                        </div>
                    </div>
                    <s:if test="validServer" >
                        <p class="sr-only">
                            <wpsf:hidden name="baseUrl" value="%{widget.getConfig().get('baseUrl')}" />
                            <wpsf:hidden name="pid" value="%{widget.getConfig().get('pid')}" />
                        </p>
                        <div class="form-group">
                            <div class="col-xs-12 col-sm-4 col-md-3 margin-small-vertical">
                                <wpsf:submit action="changeServer" type="button" cssClass="btn btn-primary btn-block">
                                    <%-- <span class="icon fa fa-floppy-o"></span>&#32;--%>
                                    <s:text name="label.changeServer" />
                                </wpsf:submit>
                            </div>
                        </div>
                    </s:if>

                </fieldset>
                <s:if test="validServer" >
                    <fieldset class="margin-base-top">
                        <legend><s:text name="title.gokibiWidget" /></legend>
                        <%--<p><s:text name="note.extraOption.intro" /></p>--%>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <label for="widgetCode"><s:text name="label.widgetCode" /></label>
                                <wpsf:select list="gokibiWidgets" name="widgetCode" id="widgetCode" listKey="key" listValue="value"
                                             value="%{widget.config.get('widgetCode')}" cssClass="form-control" />
                            </div>
                        </div>
                    </fieldset>
                </s:if>
            </div>
        </div>
        <s:if test="validServer" >
            <div class="form-group">
                <div class="col-xs-12 col-sm-4 col-md-3 margin-small-vertical">
                    <wpsf:submit action="save" type="button" cssClass="btn btn-primary btn-block">
                        <span class="icon fa fa-floppy-o"></span>&#32;
                        <s:text name="label.save" />
                    </wpsf:submit>
                </div>
            </div>
        </s:if>
        <s:else>
            <div class="form-group">
                <div class="col-xs-12 col-sm-4 col-md-3 margin-small-vertical">
                    <wpsf:submit action="configServer" type="button" cssClass="btn btn-primary btn-block">
                        <%--<span class="icon fa fa-floppy-o"></span>&#32;--%>
                        <s:text name="label.continue" />
                    </wpsf:submit>
                </div>
            </div>
        </s:else>
    </s:form>

</div>
