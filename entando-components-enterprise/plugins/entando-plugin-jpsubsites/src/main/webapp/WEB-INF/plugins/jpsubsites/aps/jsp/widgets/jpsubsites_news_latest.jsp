<%@ taglib prefix="wp" uri="/aps-core" %>
<%@ taglib prefix="ccwp" uri="/WEB-INF/comuneCagliari/aps/tld/comuneCagliari-core.tld" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="jacms" uri="/jacms-aps-core" %>
<%
/*  
	Author: William Ghelfi <w.ghelfi@agiletec.it> - 2005/05/23
	Author: Eugenio Santoboni <e.santoboni@agiletec.it>
*/  		
%>
<wp:headInfo type="CSS" info="showlets/news_latest.css" />

<div class="news_latest">
<h3 class="title"><wp:i18n key="jpsubsites_LATEST_NEW" /></h3>

<ccwp:dateContentList listName="contentList" contentType="NWS" 	attributeNameStart="DataInizioPubblicazione" attributeNameEnd="DataFinePubblicazione" 
	future="true" order="DESC" useCache="true" />
<c:if test="${contentList != null}">
	<wp:pager listName="contentList" objectName="groupContent" max="3" pagerIdFromFrame="true" >
	<ul>
	<c:forEach var="content" items="${contentList}" begin="${groupContent.begin}" end="${groupContent.end}">
		<li><div class="new"><jacms:content contentId="${content}" modelId="37" /></div></li>
	</c:forEach>
	<c:set var="groupContent" value="${groupContent}" scope="request"/>
	</ul>
	</wp:pager>
</c:if>

<wp:currentShowlet param="config" configParam="newsArchivePage" var="newsArchivePageShowletParam" />
<h4 class="archive"><a href="<wp:url page="${newsArchivePageShowletParam}" />" title="<wp:i18n key="jpsubsites_VAI_PAGINA"/>:<wp:i18n key="jpsubsites_NEWS_ARCHIVE"/>"><wp:i18n key="jpsubsites_NEWS_ARCHIVE"/></a></h4>

</div>