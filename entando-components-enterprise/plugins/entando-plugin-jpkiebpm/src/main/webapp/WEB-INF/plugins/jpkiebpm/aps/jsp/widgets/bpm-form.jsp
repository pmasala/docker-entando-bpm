<%--
  ~ /*
  ~  * The MIT License
  ~  *
  ~  * Copyright 2017 Entando Inc..
  ~  *
  ~  * Permission is hereby granted, free of charge, to any person obtaining a copy
  ~  * of this software and associated documentation files (the "Software"), to deal
  ~  * in the Software without restriction, including without limitation the rights
  ~  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  ~  * copies of the Software, and to permit persons to whom the Software is
  ~  * furnished to do so, subject to the following conditions:
  ~  *
  ~  * The above copyright notice and this permission notice shall be included in
  ~  * all copies or substantial portions of the Software.
  ~  *
  ~  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  ~  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  ~  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  ~  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  ~  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  ~  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  ~  * THE SOFTWARE.
  ~  */
--%>

<%@ taglib uri="/aps-core" prefix="wp" %>
<%@ taglib uri="/jpkiebpm-aps-core" prefix="jpkie" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jpkie:form widgetConfigInfoIdVar="configId"/>

<script src="<wp:resourceURL />plugins/jpkiebpm/static/js/jquery-ui.js"></script>

<link rel="stylesheet" href="<wp:resourceURL />plugins/jpkiebpm/static/css/jquery-ui.css" media="screen"/>
<script src="<wp:resourceURL />plugins/jpkiebpm/static/js/jquery.validate.js"></script>
<script src="<wp:resourceURL />plugins/jpkiebpm/static/js/additional-methods.js"></script>

<link rel="stylesheet" href="<wp:resourceURL />plugins/jpkiebpm/static/css/jpkie.css" media="screen"/>

<script src="<wp:resourceURL />plugins/jpkiebpm/static/js/lib/dynamic-form/jquery.dform-1.1.0.js"></script>
<script src="<wp:resourceURL />plugins/jpkiebpm/static/js/dynamic-form.js"></script>

<script>
    $(function () {
        var url = "<wp:info key="systemParam" paramName="applicationBaseURL" />api/rs/<wp:info key="currentLang"/>/jpkiebpm/bpmForm.json?configId=<c:out value="${configId}" />";
        var action = "<wp:info key="systemParam" paramName="applicationBaseURL" />api/rs/<wp:info key="currentLang"/>/jpkiebpm/bpmForm";
        org.entando.form.urlRequest = "";
        org.entando.form.loadFrom(url);
        $("#bpm-form").submit(function (event) {
            if ($(this).valid()) {
                event.preventDefault();
                delete org.entando.form.urlRequest.html;
                $("#bpm-form").serializeArray().forEach(function (element) {
                    if (element.name !== 'processId' && element.name !== 'containerId' &&
                        element.name !== 'name' && element.name !== 'post' && element.name !== 'id') {
                        if (Array.isArray(org.entando.form.urlRequest.mainForm.fields)) {
                            org.entando.form.urlRequest.mainForm.fields.forEach(function (el) {
                                el.fieldset.field.forEach(function (el1) {
                                    if (el1.name === element.name) {
                                        el1.value = element.value;
                                    }
                                })
                            })
                        }
                    }
                });
                $.ajax({
                    url: action, type: 'post', contentType: 'application/json',
                    data: JSON.stringify(org.entando.form.urlRequest),
                    dataType: 'json'

                }).done(function (result, textStatus) {

                    $("#bpm-form").empty();
                    org.entando.form.urlRequest = "";
                    org.entando.form.loadFrom(url);
                    var msg = "";
                    if (result.response.result = "SUCCESS") {
                        msg = "Process successfully started.";
                    } else {
                        msg = "Something wrong.";
                    }
                    $("#message").append(msg);
                    $("#dialog-response-process").dialog({
                        modal: true,
                        buttons: {
                            Ok: function () {
                                $(this).dialog("close");
                                location.reload();
                            }
                        }
                    });
                });
            }
        })
    });
</script>
<br><br><br>
<div class="ibox ibox-padding">
    <div class="ibox-title">
        <h3 class="control-label editLabel" id="MORTGAGE_REQUEST_PROCESS"><wp:i18n key="MORTGAGE_REQUEST_PROCESS"/></h3>
    </div>
    <div class="ibox-content">
        <div id="dialog-response-process" title="Message">
            <p>
            <div id="message"></div>
            </p>
        </div>
        <form id="bpm-form"></form>
    </div>
</div>

